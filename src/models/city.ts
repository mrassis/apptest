export class City {
  name: string;
  totalPopulation: number;
  country: string;
  zipCode: number;
}
