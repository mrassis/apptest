import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {BoilerplateService} from "../../services/boilerplate";
import {City} from "../../models/city";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  cities: Array<City>;

  constructor(
    public navCtrl: NavController,
    private boilerPlateService: BoilerplateService
  ) {

  }

  authenticate() {
    this.boilerPlateService.authenticate('password').subscribe((resp:any) => {
      this.boilerPlateService.TOKEN = JSON.parse(resp._body).token;
    }, err => {
      console.log('erro', err);
    });
  }

  getAllCities() {
    this.boilerPlateService.getAllCities().subscribe((resp:any) => {
      this.cities = JSON.parse(resp._body);
      console.log('cities', this.cities);
    }, err => {
      console.log('erro', err);
    })
  }
}
