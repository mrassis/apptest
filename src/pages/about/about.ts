import { Component } from '@angular/core';
import {NavController, ToastController} from 'ionic-angular';
import {FormBuilder} from "@angular/forms";
import {City} from "../../models/city";
import {BoilerplateService} from "../../services/boilerplate";

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  city: City;

  constructor(
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public boilerService: BoilerplateService,
    public toastCtrl: ToastController
  ) {

  }

  ionViewWillEnter() {
    this.city = new City();
  }

  createCity() {
    this.boilerService.createCity(this.city).subscribe(resp => {
      this.toastCtrl.create({
        message: 'Sucesso',
        duration: 2000
      }).present();
    }, err => {
      this.toastCtrl.create({
        message: 'Erro',
        duration: 2000
      }).present();
    })
  }
}
