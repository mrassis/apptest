import {Injectable} from "@angular/core";
import {Observable} from "rxjs";
import {Http, Headers, RequestOptions} from '@angular/http';
import {City} from "../models/city";

@Injectable()
export class BoilerplateService {
  BASE_URL = 'http://localhost:3000/';
  TOKEN = '';

  constructor(
    private http: Http
  ) {

  }

  authenticate(password) {
    let headers = new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');

    return this.post(this.BASE_URL + 'api/authenticate', {password: password}, headers);
  }

  getAllCities() {
    let headers = new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');
    headers.set('Authorization', 'Bearer ' + this.TOKEN);

    return this.get(this.BASE_URL + 'api/cities', headers);
  }

  createCity(data: City) {
    let headers = new Headers();
    headers.set('Content-Type', 'application/x-www-form-urlencoded');
    headers.set('Authorization', 'Bearer ' + this.TOKEN);

    return this.post(this.BASE_URL + 'api/cities', JSON.stringify(data), headers);
  }

  post(url, data, headers) {
    return this.http.post(url, data, {
      headers: headers
    })
  }

  get(url, headers) {
    return this.http.get(url, {
      headers: headers
    })
  }

  // createAuthorizationHeader(headers: Headers) {
  //   headers.append('Authorization', 'Bearer ' + token);
  //
  // }
}
